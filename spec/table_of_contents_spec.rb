require 'fileutils'

describe 'indent/java.vim' do
  describe '#Run test' do
    it 'Should produce the table of contents' do
      test_dir = File.expand_path('../test', __FILE__)
      system "cd #{test_dir} && jekyll build"

      File.read("#{test_dir}/_site/a/index.html").should == <<-EOF
<ul>

  <li>A Sub 1</li>

  <li>A Sub 2</li>

</ul>
      EOF

      File.read("#{test_dir}/_site/b/index.html").should == <<-EOF
<ul>

  <li>B Sub 1</li>

  <li>B Sub 2</li>

</ul>
      EOF

    end
  end
end

# vim: ts=2 sw=2 et
